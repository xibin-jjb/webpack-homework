module.exports = {
    entry: "./index.js",
    output: {
        filename: "bundle.js"
    },
    mode: "development",
    devServer: { // 自动刷新
        publicPath: "/dist"
    },
}